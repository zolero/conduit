[workspace.lints.rust]
explicit_outlives_requirements = "warn"
unused_qualifications = "warn"

[workspace.lints.clippy]
cloned_instead_of_copied = "warn"
dbg_macro = "warn"
str_to_string = "warn"

[package]
authors = ["timokoesters <timo@koesters.xyz>"]
description = "A Matrix homeserver written in Rust"
edition = "2021"
homepage = "https://conduit.rs"
license = "Apache-2.0"
name = "conduit"
readme = "README.md"
repository = "https://gitlab.com/famedly/conduit"
version = "0.8.0-alpha"

# See also `rust-toolchain.toml`
rust-version = "1.78.0"

# See more keys and their definitions at https://doc.rust-lang.org/cargo/reference/manifest.html

[lints]
workspace = true

[dependencies]
# Web framework
axum = { version = "0.6.18", default-features = false, features = [
  "form",
  "headers",
  "http1",
  "http2",
  "json",
  "matched-path",
], optional = true }
axum-server = { version = "0.5.1", features = ["tls-rustls"] }
tower = { version = "0.4.13", features = ["util"] }
tower-http = { version = "0.4.1", features = [
  "add-extension",
  "cors",
  "sensitive-headers",
  "trace",
  "util",
] }

# Used for matrix spec type definitions and helpers
#ruma = { version = "0.4.0", features = ["compat", "rand", "appservice-api-c", "client-api", "federation-api", "push-gateway-api-c", "state-res", "unstable-pre-spec", "unstable-exhaustive-types"] }
ruma = { git = "https://github.com/ruma/ruma", rev = "5495b85aa311c2805302edb0a7de40399e22b397", features = [
  "appservice-api-c",
  "client-api",
  "compat",
  "federation-api",
  "push-gateway-api-c",
  "rand",
  "ring-compat",
  "state-res",
  "unstable-exhaustive-types",
  "unstable-msc2448",
  "unstable-msc3575",
  "unstable-unspecified",
] }
#ruma = { git = "https://github.com/timokoesters/ruma", rev = "4ec9c69bb7e09391add2382b3ebac97b6e8f4c64", features = ["compat", "rand", "appservice-api-c", "client-api", "federation-api", "push-gateway-api-c", "state-res", "unstable-msc2448", "unstable-msc3575", "unstable-exhaustive-types", "ring-compat", "unstable-unspecified" ] }
#ruma = { path = "../ruma/crates/ruma", features = ["compat", "rand", "appservice-api-c", "client-api", "federation-api", "push-gateway-api-c", "state-res", "unstable-msc2448", "unstable-msc3575", "unstable-exhaustive-types", "ring-compat", "unstable-unspecified" ] }

# Async runtime and utilities
tokio = { version = "1.28.1", features = ["fs", "macros", "signal", "sync"] }
# Used for storing data permanently
#sled = { version = "0.34.7", features = ["compression", "no_metrics"], optional = true }
#sled = { git = "https://github.com/spacejam/sled.git", rev = "e4640e0773595229f398438886f19bca6f7326a2", features = ["compression"] }
persy = { version = "1.4.4", optional = true, features = ["background_ops"] }

# Used for the http request / response body type for Ruma endpoints used with reqwest
bytes = "1.4.0"
http = "0.2.9"
# Used to find data directory for default db path
directories = "5"
# Used for ruma wrapper
serde_json = { version = "1.0.96", features = ["raw_value"] }
# Used for appservice registration files
serde_yaml = "0.9.21"
# Used for pdu definition
serde = { version = "1.0.163", features = ["rc"] }
# Used for secure identifiers
rand = "0.8.5"
# Used to hash passwords
rust-argon2 = "2"
# Used to send requests
hyper = "0.14.26"
reqwest = { version = "0.11.18", default-features = false, features = [
  "rustls-tls-native-roots",
  "socks",
] }
# Used for conduit::Error type
thiserror = "1.0.40"
# Used to generate thumbnails for images
image = { version = "0.25", default-features = false, features = [
  "gif",
  "jpeg",
  "png",
] }
# Used to encode server public key
base64 = "0.22"
# Used when hashing the state
ring = "0.17.7"
# Used when querying the SRV record of other servers
hickory-resolver = "0.24"
# Used to find matching events for appservices
regex = "1.8.1"
# jwt jsonwebtokens
jsonwebtoken = "9.2.0"
# Performance measurements
opentelemetry = { version = "0.18.0", features = ["rt-tokio"] }
opentelemetry-jaeger = { version = "0.17.0", features = ["rt-tokio"] }
tracing = { version = "0.1.37", features = [] }
tracing-flame = "0.2.0"
tracing-opentelemetry = "0.18.0"
tracing-subscriber = { version = "0.3.17", features = ["env-filter"] }

lru-cache = "0.1.2"
parking_lot = { version = "0.12.1", optional = true }
rusqlite = { version = "0.31", optional = true, features = ["bundled"] }

# crossbeam = { version = "0.8.2", optional = true }
num_cpus = "1.15.0"
threadpool = "1.8.1"
# heed = { git = "https://github.com/timokoesters/heed.git", rev = "f6f825da7fb2c758867e05ad973ef800a6fe1d5d", optional = true }
# Used for ruma wrapper
serde_html_form = "0.2.0"

thread_local = "1.1.7"
# used for TURN server authentication
hmac = "0.12.1"
sha-1 = "0.10.1"
# used for conduit's CLI and admin room command parsing
clap = { version = "4.3.0", default-features = false, features = [
  "derive",
  "error-context",
  "help",
  "std",
  "string",
  "usage",
] }
futures-util = { version = "0.3.28", default-features = false }
# Used for reading the configuration from conduit.toml & environment variables
figment = { version = "0.10.8", features = ["env", "toml"] }

# Validating urls in config
url = { version = "2", features = ["serde"] }

async-trait = "0.1.68"
tikv-jemallocator = { version = "0.5.0", features = [
  "unprefixed_malloc_on_supported_platforms",
], optional = true }

sd-notify = { version = "0.4.1", optional = true }

[dependencies.rocksdb]
features = ["lz4", "multi-threaded-cf", "zstd"]
optional = true
package = "rust-rocksdb"
version = "0.25"

[target.'cfg(unix)'.dependencies]
nix = { version = "0.28", features = ["resource"] }

[features]
default = ["backend_rocksdb", "backend_sqlite", "conduit_bin", "systemd"]
#backend_sled = ["sled"]
backend_persy = ["parking_lot", "persy"]
backend_sqlite = ["sqlite"]
#backend_heed = ["heed", "crossbeam"]
backend_rocksdb = ["rocksdb"]
conduit_bin = ["axum"]
jemalloc = ["tikv-jemallocator"]
sqlite = ["parking_lot", "rusqlite", "tokio/signal"]
systemd = ["sd-notify"]

[[bin]]
name = "conduit"
path = "src/main.rs"
required-features = ["conduit_bin"]

[lib]
name = "conduit"
path = "src/lib.rs"

[package.metadata.deb]
assets = [
  [
    "README.md",
    "usr/share/doc/matrix-conduit/",
    "644",
  ],
  [
    "debian/README.md",
    "usr/share/doc/matrix-conduit/README.Debian",
    "644",
  ],
  [
    "target/release/conduit",
    "usr/sbin/matrix-conduit",
    "755",
  ],
]
conf-files = ["/etc/matrix-conduit/conduit.toml"]
copyright = "2020, Timo Kösters <timo@koesters.xyz>"
depends = "$auto, ca-certificates"
extended-description = """\
A fast Matrix homeserver that is optimized for smaller, personal servers, \
instead of a server that has high scalability."""
license-file = ["LICENSE", "3"]
maintainer = "Paul van Tilburg <paul@luon.net>"
maintainer-scripts = "debian/"
name = "matrix-conduit"
priority = "optional"
section = "net"
systemd-units = { unit-name = "matrix-conduit" }

[profile.dev]
incremental = true
lto = 'off'

[profile.release]
codegen-units = 32
incremental = true
lto = 'thin'
# If you want to make flamegraphs, enable debug info:
# debug = true

# For releases also try to max optimizations for dependencies:
[profile.release.build-override]
opt-level = 3
[profile.release.package."*"]
opt-level = 3
